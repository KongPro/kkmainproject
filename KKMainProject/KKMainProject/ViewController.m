//
//  ViewController.m
//  KKMainProject
//
//  Created by 孔繁武 on 2021/6/28.
//

#import "ViewController.h"
#import <KKMediator+KK_Home.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
    view.backgroundColor = [UIColor redColor];
    [self.view addSubview:view];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UIViewController *controller = [[KKMediator sharedInstance] kkHome_indexPage];
    [self.navigationController pushViewController:controller animated:YES];
}


@end
