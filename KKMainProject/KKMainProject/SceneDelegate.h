//
//  SceneDelegate.h
//  KKMainProject
//
//  Created by 孔繁武 on 2021/6/28.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

