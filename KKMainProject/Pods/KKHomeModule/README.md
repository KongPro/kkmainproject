# KKHomeModule

[![CI Status](https://img.shields.io/travis/KongPro@163.com/KKHomeModule.svg?style=flat)](https://travis-ci.org/KongPro@163.com/KKHomeModule)
[![Version](https://img.shields.io/cocoapods/v/KKHomeModule.svg?style=flat)](https://cocoapods.org/pods/KKHomeModule)
[![License](https://img.shields.io/cocoapods/l/KKHomeModule.svg?style=flat)](https://cocoapods.org/pods/KKHomeModule)
[![Platform](https://img.shields.io/cocoapods/p/KKHomeModule.svg?style=flat)](https://cocoapods.org/pods/KKHomeModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KKHomeModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KKHomeModule'
```

## Author

KongPro@163.com, xxxx---

## License

KKHomeModule is available under the MIT license. See the LICENSE file for more info.
