
# KKMediatorModule

[![CI Status](https://img.shields.io/travis/KongPro@163.com/KKMediatorModule.svg?style=flat)](https://travis-ci.org/KongPro@163.com/KKMediatorModule)
[![Version](https://img.shields.io/cocoapods/v/KKMediatorModule.svg?style=flat)](https://cocoapods.org/pods/KKMediatorModule)
[![License](https://img.shields.io/cocoapods/l/KKMediatorModule.svg?style=flat)](https://cocoapods.org/pods/KKMediatorModule)
[![Platform](https://img.shields.io/cocoapods/p/KKMediatorModule.svg?style=flat)](https://cocoapods.org/pods/KKMediatorModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KKMediatorModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KKMediatorModule'
```

## Author

KongPro@163.com, xxxx---

## License

KKMediatorModule is available under the MIT license. See the LICENSE file for more info.

