//
//  KKMediator.h
//  KKMediatorModule_Example
//
//  Created by 孔繁武 on 2021/6/28.
//  Copyright © 2021 KongPro@163.com. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KKMediator : NSObject

+ (instancetype _Nonnull)sharedInstance;

// 远程App调用入口
//- (id _Nullable)performActionWithUrl:(NSURL * _Nullable)url completion:(void(^_Nullable)(NSDictionary * _Nullable info))completion;
// 本地组件调用入口
- (id _Nullable )performTarget:(NSString * _Nullable)targetName action:(NSString * _Nullable)actionName params:(NSDictionary * _Nullable)params shouldCacheTarget:(BOOL)shouldCacheTarget;

@end

NS_ASSUME_NONNULL_END
