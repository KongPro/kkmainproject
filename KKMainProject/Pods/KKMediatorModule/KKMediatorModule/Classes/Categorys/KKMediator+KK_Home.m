//
//  KKMediator+KK_Home.m
//  KKMediatorModule_Example
//
//  Created by 孔繁武 on 2021/6/28.
//  Copyright © 2021 KongPro@163.com. All rights reserved.
//

#import "KKMediator+KK_Home.h"

@implementation KKMediator (KK_Home)

- (UIViewController *)kkHome_indexPage{
    
    UIViewController *viewController = [self kkHome_performAction:@"indexPage" params:nil shouldCacheTarget:NO];
    return viewController;
    
    // 定义一个repo  类名为KKHomeTarget 方法名字为kkAction_indexPage:
}


- (id)kkHome_performAction:(NSString *)actionName params:(NSDictionary *)params shouldCacheTarget:(BOOL)shouldCacheTarget{
    return [self performTarget:@"Home" action:actionName params:params shouldCacheTarget:shouldCacheTarget];
}

@end
