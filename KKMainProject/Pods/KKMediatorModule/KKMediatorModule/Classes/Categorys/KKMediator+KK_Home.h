//
//  KKMediator+KK_Home.h
//  KKMediatorModule_Example
//
//  Created by 孔繁武 on 2021/6/28.
//  Copyright © 2021 KongPro@163.com. All rights reserved.
//

#import "KKMediator.h"

NS_ASSUME_NONNULL_BEGIN

@interface KKMediator (KK_Home)

- (UIViewController *)kkHome_indexPage;

@end

NS_ASSUME_NONNULL_END
